Dev Exam: (PHP, Mongodb, Docker, Git, JavaScript)


1. Create an API that runs a database query that groups collection data by status and date time (datasource: mongodb://guest:password_will_be_provided@54.251.133.139:27017/?authSource=logs_db)

2. Parse query result and return JSON data with same format below:

    `{
        "series": ['01:05', '01:10', '01:15'],
        "success": [4,3,2],
        "error": [2,0,1]
    }`

3. Create a simple line chart to visualize generated data **(Optional but a plus)**
4. Create a git repository containing your code, Dockerfile and docker-compose.yml
